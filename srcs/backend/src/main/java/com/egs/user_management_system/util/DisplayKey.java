package com.egs.user_management_system.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource(ignoreResourceNotFound = true, value = "classpath:display.properties")
public class DisplayKey {

    @Autowired
    private Environment environment;

    public String getValue(String key) {
        return environment.getProperty(key);
    }
}
