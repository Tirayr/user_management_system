package com.egs.user_management_system.repository;

import com.egs.user_management_system.model.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long> {

    @Query(value = "SELECT * FROM egs.address WHERE egs.address.country = :country and egs.address.city = :city and egs.address.street = :street"
            , nativeQuery = true)
    List<Address> findByParams(String country, String city, String street);
}
