package com.egs.user_management_system.repository;

import com.egs.user_management_system.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM egs.user WHERE user.email = :email",
            nativeQuery = true)
    User findByEmail(@Param("email") String email);

    @Query(value = "SELECT * FROM egs.user WHERE user.id = :id",
            nativeQuery = true)
    User getUserById(@Param("id") long id);

    @Query(value = "SELECT * FROM egs.user WHERE user.status = :status",
            nativeQuery = true)
    ArrayList<User> getDisabledUsers(@Param("status") String status);

    @Query(value = "SELECT * FROM egs.user WHERE user.addres_id = :addressId",
            nativeQuery = true)
    List<User> findByAddress(Long addressId);
}