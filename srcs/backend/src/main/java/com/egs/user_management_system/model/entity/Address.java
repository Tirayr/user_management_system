package com.egs.user_management_system.model.entity;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Entity
@Getter
@Setter
public class Address extends BaseEntity implements Cloneable {

    @NotNull
    private String city;
    @NotNull
    private String country;
    @NotNull
    private String street;
    @NotNull
    private String zipCode;
    @NotNull
    private String apartment;

    @Override
    public Address clone() throws CloneNotSupportedException {
        return (Address) super.clone();
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", street='" + street + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", apartment='" + apartment + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return this.hashCode() == o.hashCode() && Objects.equals(city, address.city) && Objects.equals(country, address.country) && Objects.equals(street, address.street) && Objects.equals(zipCode, address.zipCode) && Objects.equals(apartment, address.apartment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, country, street, zipCode, apartment);
    }
}