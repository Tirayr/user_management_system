package com.egs.user_management_system.model.entity;

import com.egs.user_management_system.model.Role;
import com.egs.user_management_system.model.Status;
import com.egs.user_management_system.util.annotation.Password;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/*
 *  Create table in DB by class User.
 *
 * */
@Entity
@Setter
public class User extends BaseEntity {
    @NotNull
    @Getter
    private String firstName;

    @NotNull
    @Getter
    private String lastName;

    @NotNull
    @Getter
    @Enumerated(EnumType.STRING)
    private Role role;

    @NotNull
    @Getter
    @Enumerated(EnumType.STRING)
    private Status status;

    @NonNull
    @Getter
    @Email(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
    @Column(unique = true)
    private String email;

    @NotNull
    @Getter
    @Password
    private String password;

    @ManyToOne(cascade = {CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST})
    private Address address;

    public Address getAddress() throws CloneNotSupportedException {
        return address.clone();
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(role.name()));
        return authorities;
    }

}