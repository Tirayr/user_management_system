package com.egs.user_management_system.config.jwt;

import com.egs.user_management_system.model.Role;
import com.egs.user_management_system.model.entity.User;
import com.egs.user_management_system.service.UserService;
import com.egs.user_management_system.util.exception.UserNotFoundException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
public class JwtProvider implements AuthenticationProvider {

    private String secret = "task";

    private long expTime = 3600000;

    @Autowired
    private UserService userService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @PostConstruct
    protected void init() {
        secret = Base64.getEncoder().encodeToString(secret.getBytes());

    }

    /*
     *   Create Token
     * */
    public String createToken(String email, String role) {
        Claims claims = Jwts.claims().setSubject(email);
        claims.put("roles", role);

        Date now = new Date();
        Date validity = new Date(now.getTime() + expTime);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /*
     *    Resolve Token
     * */
    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("token");
        if (bearerToken != null && bearerToken.startsWith("Bearer_")) {
            return bearerToken.substring(7);
        }
        return bearerToken;
    }


    /*
     *    Validate Token
     * */
    public boolean validateToken(String token) {
        Jws<Claims> claims = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token);
        if (claims.getBody().getExpiration().before(new Date())) {
            return false;
        }
        return true;
    }

    /*
     *  get email
     * */
    public String getEmail(String token) {
        return Jwts
                .parser().setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody().getSubject();

    }


    /*
     *    Get Aunthentication
     * */
    public Authentication getAuthentication(String token) throws UserNotFoundException {
        User user = userService.getByEmail(getEmail(token));
        Role role = user.getRole();
        return new UsernamePasswordAuthenticationToken(user, role, user.getAuthorities());
    }

    /*
     *    Authentication
     * */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String passeord = (String) authentication.getCredentials();
        User user = null;
        try {
            user = userService.getByEmail(email);
        } catch (UserNotFoundException e) {
            throw new RuntimeException(e);
        }
        return new UsernamePasswordAuthenticationToken(user, passeord);
    }


    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
