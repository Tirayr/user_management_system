package com.egs.user_management_system.util.validator;

import com.egs.user_management_system.model.dto.AddressDto;
import com.egs.user_management_system.model.dto.UserDto;
import com.egs.user_management_system.util.DisplayKey;
import com.egs.user_management_system.util.exception.InvalidValuePassedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RequestValidator {

    @Autowired
    private DisplayKey displayKey;

    /*
     *
     *
     * */
    public void validateEmptyData(UserDto dto) {
        StringBuilder exeptionMessageBuilderr = new StringBuilder();
        if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) exeptionMessageBuilderr.append("firstName ");
        if (dto.getLastName() == null || dto.getLastName().isEmpty()) exeptionMessageBuilderr.append("lastName ");
        if (dto.getEmail() == null || dto.getEmail().isEmpty()) exeptionMessageBuilderr.append("email ");
        if (dto.getPassword() == null || dto.getPassword().isEmpty()) exeptionMessageBuilderr.append("password ");
        try {
            if (dto.getAddress() == null) exeptionMessageBuilderr.append("address ");
            else {
                AddressDto address = dto.getAddress();
                if (address.getCountry() == null || address.getCountry().isEmpty())
                    exeptionMessageBuilderr.append("address.country ");
                if (address.getCity() == null || address.getCity().isEmpty())
                    exeptionMessageBuilderr.append("address.city ");
                if (address.getStreet() == null || address.getStreet().isEmpty())
                    exeptionMessageBuilderr.append("address.street ");
                if (address.getApartment() == null || address.getApartment().isEmpty())
                    exeptionMessageBuilderr.append("address.apartment ");
                if (address.getZipCode() == null || address.getZipCode().isEmpty())
                    exeptionMessageBuilderr.append("address.zipCode ");
            }
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }

        if (!exeptionMessageBuilderr.isEmpty()) {
            exeptionMessageBuilderr.insert(0,
                    displayKey.getValue("INVALID_VALUE") + " " + displayKey.getValue("EMPTY_VALUES") + " ");
            throw new InvalidValuePassedException(exeptionMessageBuilderr.toString());
        }
    }
}
