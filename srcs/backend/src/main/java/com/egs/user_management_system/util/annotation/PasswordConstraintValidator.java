package com.egs.user_management_system.util.annotation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.passay.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PasswordConstraintValidator implements ConstraintValidator<Password, String> {

    @Override
    /*
     * Password validation conditions
     * min length 6
     * should contain capital letter(at least one)
     * special character(at least one),allowed characters(!, #,$,%,*)
     * least 1number.
     *
     * */
    public boolean isValid(String password, ConstraintValidatorContext context) {
        LengthRule lengthRule = new LengthRule();
        lengthRule.setMinimumLength(6);

        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                lengthRule,
                new UppercaseCharacterRule(1),
                new DigitCharacterRule(1),
                new CustomSpecialCharacterRule(1)
        ));

        RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }
        List<String> messages = validator.getMessages(result);

        String messageTemplate = messages.stream()
                .collect(Collectors.joining(","));
        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
        return false;
    }
}
