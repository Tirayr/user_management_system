package com.egs.user_management_system.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDto implements Cloneable {

    private String country;
    private String city;
    private String street;
    private String apartment;
    private String zipCode;


    public AddressDto clone() throws CloneNotSupportedException {
        return (AddressDto) super.clone();
    }

    public AddressDto(){

    }

    public AddressDto(String country, String city, String street, String apartment, String zipCode) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.apartment = apartment;
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "AddressDto{" +
                "city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", street='" + street + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", apartment='" + apartment + '\'' +
                '}';
    }
}
