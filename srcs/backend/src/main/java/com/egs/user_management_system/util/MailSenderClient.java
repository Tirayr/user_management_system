package com.egs.user_management_system.util;

import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Base64;
import java.util.Properties;

@Component
public class MailSenderClient {


    public final String MY_EMAIL = "";// input your email, this is email addres hwo shuld be send messages ;

    public final String MY_PASSWORD = "";// input your email password or secretCode;


    public void send(String to, String subject, String content) throws MessagingException {

        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");


        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(MY_EMAIL, MY_PASSWORD);
            }
        });


        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(MY_EMAIL));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
        message.setSubject(subject);
        message.setText(content + " " + "http://localhost:4200/confirmation/" + Base64.getEncoder().encodeToString(to.getBytes()));

        Transport.send(message);

    }
}
