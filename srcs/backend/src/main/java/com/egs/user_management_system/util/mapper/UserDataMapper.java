package com.egs.user_management_system.util.mapper;

import com.egs.user_management_system.model.Role;
import com.egs.user_management_system.model.Status;
import com.egs.user_management_system.model.dto.AddressDto;
import com.egs.user_management_system.model.dto.UserDto;
import com.egs.user_management_system.model.entity.Address;
import com.egs.user_management_system.model.entity.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDataMapper {

    public static UserDto mapSuccessResponseDto(UserDto dto) {
        String firstName = capitalizeIfNedde(dto.getFirstName());
        String lastName = capitalizeIfNedde(dto.getFirstName());
        dto.setFirstName(firstName);
        dto.setLastName(lastName);
        dto.setRole(Role.USER);
        dto.setStatus(Status.DISABLED);
        dto.setPassword(null);

        return dto;
    }

    public static User mapDtoToUser(UserDto dto, List<Address> addressList) {
        try {
            User user = new User();

            String firstName = capitalizeIfNedde(dto.getFirstName());
            user.setFirstName(firstName);
            String lastName = capitalizeIfNedde(dto.getLastName());
            user.setLastName(lastName);
            if (dto.getStatus() == null) { // If user created by registration flow, the Status shuld be DISABLED before confirmation.
                user.setStatus(Status.DISABLED);
            } else user.setStatus(dto.getStatus()); // When user created by Admin, the status set ENABLED.
            if (dto.getRole() == null) { // If user created by registration flow, the Role shuld be USER by defoult.
                user.setRole(Role.USER);
            } else user.setRole(dto.getRole()); // When user created by Admin, the status set Admin user.
            user.setEmail(dto.getEmail());
            user.setPassword(dto.getPassword());
            Address address = AddressDataMapper.mapDtoToAddress(dto.getAddress());
            if (!addressList.isEmpty() && addressList.contains(address)) addressList.forEach(addr -> {
                if (addr.equals(address)) user.setAddress(addr);
            });
            else user.setAddress(AddressDataMapper.mapDtoToAddress(dto.getAddress()));

            return user;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw e;
        }
    }

    private static String capitalizeIfNedde(String input) {
        return Character.isUpperCase(input.charAt(0))
                ? input
                : input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    public static UserDto mapUserToDto(User user) throws CloneNotSupportedException {
        AddressDto addressDto = AddressDataMapper.mapAddressToDto(user.getAddress());
        return new UserDto(user.getFirstName()
                , user.getLastName()
                , user.getEmail()
                , user.getRole()
                , user.getStatus()
                , addressDto);
    }
}
