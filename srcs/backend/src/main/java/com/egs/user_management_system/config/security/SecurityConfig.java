package com.egs.user_management_system.config.security;


import com.egs.user_management_system.config.jwt.JwtConfig;
import com.egs.user_management_system.config.jwt.JwtFilter;
import com.egs.user_management_system.config.jwt.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {
    @Autowired
    private final JwtProvider jwtProvider;

    public SecurityConfig(JwtProvider jwtProvider, JwtFilter jwtFilter) {
        this.jwtProvider = jwtProvider;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf(c -> c.disable())
                .authorizeHttpRequests((authz) -> authz
                        .requestMatchers("/user/**").permitAll()// access for ROLE_USER and ROLE_ADMIN
                        .requestMatchers("/admin/**").hasRole("ADMIN")// access for only ROLE_ADMIN
                        .anyRequest().authenticated()
                )
                .apply(new JwtConfig(jwtProvider));

        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

}