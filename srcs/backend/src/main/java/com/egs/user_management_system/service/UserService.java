package com.egs.user_management_system.service;

import com.egs.user_management_system.config.jwt.JwtProvider;
import com.egs.user_management_system.model.Role;
import com.egs.user_management_system.model.dto.UserDto;
import com.egs.user_management_system.model.entity.User;
import com.egs.user_management_system.util.exception.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public interface UserService {

    User getByEmail(String email) throws UserNotFoundException;

    void checkByEmailAndsendCinfirmationMessage(UserDto userDto);

    String getToken(String email, Role role, JwtProvider jwtProvider);

    User getUserByToken(String token, JwtProvider jwtProvider) throws UserNotFoundException;

    UserDto enableUserByEmail(String email);

    ArrayList<User> getDisabledUsers(String status);

    void removeUser(User user);

    void addUsersFromCSV(List<UserDto> dtoList);

    void deleteByEmail(String email) throws UserNotFoundException;

    void update(UserDto userDto) throws UserNotFoundException;
}
