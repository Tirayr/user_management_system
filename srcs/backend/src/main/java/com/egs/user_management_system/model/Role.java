package com.egs.user_management_system.model;

public enum Role {
    USER, ADMIN
}
