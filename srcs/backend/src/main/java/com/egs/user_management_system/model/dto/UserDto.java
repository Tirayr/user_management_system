package com.egs.user_management_system.model.dto;


import com.egs.user_management_system.model.Role;
import com.egs.user_management_system.model.Status;
import lombok.Getter;
import lombok.Setter;

@Setter
public class UserDto {

    @Getter
    private String firstName;

    @Getter
    private String lastName;

    @Getter
    private String email;

    @Getter
    private Role role;

    @Getter
    private Status status;

    @Getter
    private String password;

    @Getter
    private AddressDto address;

    public AddressDto getAddress() throws CloneNotSupportedException {
        return address.clone();
    }

    public UserDto(){

    }

    public UserDto(String firstName, String lastName, String email, Role role, Status status, AddressDto address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.status = status;
        this.address = address;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                ", status=" + status +
                ", password='" + password + '\'' +
                ", address=" + address +
                '}';
    }
}