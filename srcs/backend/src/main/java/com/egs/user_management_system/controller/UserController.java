package com.egs.user_management_system.controller;

import com.egs.user_management_system.config.jwt.JwtProvider;
import com.egs.user_management_system.model.dto.ApiResponse;
import com.egs.user_management_system.model.dto.LoginUserDto;
import com.egs.user_management_system.model.dto.UserDto;
import com.egs.user_management_system.model.entity.User;
import com.egs.user_management_system.service.impl.UserServiceImpl;
import com.egs.user_management_system.util.exception.UserNotFoundException;
import com.egs.user_management_system.util.mapper.UserDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping(value = "/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserServiceImpl service;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtProvider jwtProvider;


    /*
     * Handle request by url /user/createComfirmation.
     * Validate missing and empty data, send confirmation message by given email address,
     * save data with status DISABLED.
     *
     * @RequestBody UserDto userDtoList
     * @Return ApiResponse<String>
     * */
    @PostMapping(value = "/createConfirmation")
    @Transactional
    public ApiResponse<String> createConfirmation(@RequestBody UserDto userDto) {
        service.checkByEmailAndsendCinfirmationMessage(userDto);
        logger.info("The message sent by given email for user: ", userDto);
        String responseMessage = "Dear " + userDto.getFirstName() + " " + userDto.getLastName() + ", please chek your email." +
                "Confirmation sent by given email";
        return new ApiResponse<>(HttpStatus.OK.value(), "Confirmation sent success", responseMessage);
    }

    /*
     * Handle request by url /user/enable.
     * Get user by email and set status ENABLED
     *
     * @RequestParam String email
     * @Return ApiResponse<UserDto>
     * */
    @PostMapping(value = "/enable")
    @Transactional
    public ApiResponse<UserDto> enableUser(@RequestBody String email) {
        UserDto userDto = service.enableUserByEmail(email);
        logger.info("Enable uder: ", userDto);

        return new ApiResponse<>(HttpStatus.OK.value(), "User is enabled", userDto);
    }

    /*
     * Handle request by url /user/signIn.
     * Sign in user by email/password
     *
     * @RequestBody LoginUserDto loginUserDto
     * @Return TokenDto
     * */
    @PostMapping(value = "/genrateToken")
    public ApiResponse<String> login(@RequestBody LoginUserDto loginUserDto) throws UserNotFoundException {
        logger.info("getting token via email and sign in with email and password");

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUserDto.getEmail(), loginUserDto.getPassword()));
        User user = service.getByEmail(loginUserDto.getEmail());
        String token = service.getToken(loginUserDto.getEmail(), user.getRole(), jwtProvider);

        return new ApiResponse<>(200, "success", token);
    }

    /*
     * Get user via token
     *
     * @RequestHeader String token
     * @Return ResponseEntity
     * */
    @GetMapping("/signIn")
    public ApiResponse<UserDto> getUser(@RequestHeader("token") String token) throws CloneNotSupportedException, UserNotFoundException {
        logger.info("geting user via token", token);
        User user = service.getUserByToken(token, jwtProvider);
        UserDto userDto = UserDataMapper.mapUserToDto(user);
        return new ApiResponse<>(200, "success", userDto);
    }
}