package com.egs.user_management_system.model;

public enum Status {
    ENABLED, DISABLED;
}
