package com.egs.user_management_system.util;

import com.egs.user_management_system.model.Status;
import com.egs.user_management_system.model.entity.User;
import com.egs.user_management_system.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Component
public class ScheduleProcess {

    @Autowired
    private UserServiceImpl service;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(ScheduleProcess.class);

    @Scheduled(fixedRate = 3600000) // 1 hour by milliseconds
    @Transactional
    public void reportCurrentTime() {
        logger.info("Scheduler: ***** START WORK ***** the time is now {}", dateFormat.format(new Date()));
        ArrayList<User> userList = service.getDisabledUsers(Status.DISABLED.name());
        if (!userList.isEmpty()) {
            userList.forEach(user -> {
                logger.info("Scheduler: remve user" + user, dateFormat.format(new Date()));
                service.removeUser(user);
            });
        }
        logger.info("Scheduler: ***** END WORK *****: the time is now {}", dateFormat.format(new Date()));
    }
}