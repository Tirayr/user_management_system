package com.egs.user_management_system.controller;

import com.egs.user_management_system.model.dto.ApiResponse;
import com.egs.user_management_system.model.dto.CSVFileDto;
import com.egs.user_management_system.model.dto.UserDto;
import com.egs.user_management_system.service.impl.AdminServiceImpl;
import com.egs.user_management_system.util.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping(value = "/admin")
public class AdminController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private AdminServiceImpl service;

    /*
     * Handle request by url /admin/scv.
     * Validate missing and empty data, save data with status ENABLED.
     *
     * @RequestBody File csv
     * @RequestHeader String token
     * @Return ApiResponse<List<UserDto>
     * */
    @PostMapping(value = "/csv")
    @Transactional
    public ApiResponse<List<UserDto>> createConfirmation(@RequestHeader("token") String token, @RequestBody CSVFileDto csv) {
        List<UserDto> dtoList = service.createUsersFromCSVFile(csv.getFile());
        logger.info("Add users from CSV file");
        return new ApiResponse<>(HttpStatus.OK.value(), "Success", dtoList);
    }


    /*
     * Handle request by url /admin/delete.
     * Delete user by email
     *
     * @RequestParam String email
     * @Return ApiResponse<UserDto>
     * */
    @PostMapping(value = "/delete")
    @Transactional
    public ApiResponse<String> deleteByEmail(@RequestBody String email) throws UserNotFoundException {
        service.delete(email);
        logger.info("Deleted user by email: ", email);
        String responseMessage = "Deleted user by email: " + email;
        return new ApiResponse<>(HttpStatus.OK.value(), "Success", responseMessage);
    }

    /*
     * Handle request by url /admin/update.
     * Validate, update and save data by email
     *
     * @RequestBody UserDto userDtoList
     * @Return ApiResponse<String>
     * */
    @PostMapping(value = "/update")
    @Transactional
    public ApiResponse<String> createConfirmation(@RequestBody UserDto userDto) throws UserNotFoundException {
        service.update(userDto);
        logger.info("The message sent by given email for user: ", userDto);
        String responseMessage = "Update user by email: " + userDto.getEmail();
        return new ApiResponse<>(HttpStatus.OK.value(), "Success", responseMessage);
    }
}
