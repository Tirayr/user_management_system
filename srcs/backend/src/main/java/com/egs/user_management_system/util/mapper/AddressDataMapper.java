package com.egs.user_management_system.util.mapper;

import com.egs.user_management_system.model.dto.AddressDto;
import com.egs.user_management_system.model.entity.Address;
import org.springframework.stereotype.Component;

@Component
public class AddressDataMapper {

    public static Address mapDtoToAddress(AddressDto dto) {
        Address address = new Address();
        address.setCountry(dto.getCountry());
        address.setCity(dto.getCity());
        address.setStreet(dto.getStreet());
        address.setApartment(dto.getApartment());
        address.setZipCode(dto.getZipCode());

        return address;
    }

    public static AddressDto mapAddressToDto(Address address) {
        return new AddressDto(address.getCountry()
                , address.getCity()
                , address.getStreet()
                , address.getApartment()
                , address.getZipCode());
    }
}
