package com.egs.user_management_system.service.impl;

import com.egs.user_management_system.model.dto.AddressDto;
import com.egs.user_management_system.model.dto.UserDto;
import com.egs.user_management_system.service.AdminService;
import com.egs.user_management_system.util.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private UserServiceImpl userService;

    @Override
    public List<UserDto> createUsersFromCSVFile(File scv) {
        ArrayList<UserDto> dtoList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(scv))) {
            while ((br.readLine()) != null) {
                String line = br.readLine();
                if (line.isEmpty()) {
                    String[] data = line.split(", ");
                    UserDto userDto = new UserDto();
                    userDto.setFirstName(data[0]);
                    userDto.setLastName(data[1]);
                    userDto.setEmail(data[2]);
                    userDto.setPassword(data[3]);
                    AddressDto addressDto = new AddressDto(data[4], data[5], data[6], data[7], data[8]);
                    userDto.setAddress(addressDto);
                    dtoList.add(userDto);
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        userService.addUsersFromCSV(dtoList);
        return dtoList;
    }

    @Override
    public void delete(String email) throws UserNotFoundException {
        userService.deleteByEmail(email);
    }

    @Override
    public void update(UserDto userDto) throws UserNotFoundException {
        userService.update(userDto);
    }
}
