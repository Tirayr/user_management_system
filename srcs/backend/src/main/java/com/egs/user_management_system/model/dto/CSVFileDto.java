package com.egs.user_management_system.model.dto;

import java.io.File;

public class CSVFileDto {

    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
