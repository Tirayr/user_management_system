package com.egs.user_management_system.service.impl;

import com.egs.user_management_system.config.jwt.JwtProvider;
import com.egs.user_management_system.model.Role;
import com.egs.user_management_system.model.Status;
import com.egs.user_management_system.model.dto.AddressDto;
import com.egs.user_management_system.model.dto.UserDto;
import com.egs.user_management_system.model.entity.Address;
import com.egs.user_management_system.model.entity.User;
import com.egs.user_management_system.repository.AddressRepository;
import com.egs.user_management_system.repository.UserRepository;
import com.egs.user_management_system.service.UserService;
import com.egs.user_management_system.util.DisplayKey;
import com.egs.user_management_system.util.MailSenderClient;
import com.egs.user_management_system.util.exception.DublicateEmailException;
import com.egs.user_management_system.util.exception.InvalidValuePassedException;
import com.egs.user_management_system.util.exception.UserNotFoundException;
import com.egs.user_management_system.util.mapper.UserDataMapper;
import com.egs.user_management_system.util.validator.RequestValidator;
import jakarta.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private DisplayKey displayKey;

    @Autowired
    private MailSenderClient mailSenderClient;

    @Autowired
    private RequestValidator requestValidator;

    @Override
    public void addUsersFromCSV(List<UserDto> dtoList) {
        dtoList.forEach(userDto -> {
            User user = validateAndMap(userDto);
            user.setStatus(Status.ENABLED);
            repository.save(user);
        });
    }

    @Override
    public void deleteByEmail(String email) throws UserNotFoundException {
        try {
            User user = getByEmail(email);
            Long address_id = user.getAddress().getId();
            List<User> userListByAddress = repository.findByAddress(address_id);
            if (userListByAddress.isEmpty()) {
                addressRepository.deleteById(address_id);
            }
            repository.delete(user);
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void update(UserDto userDto) throws UserNotFoundException {
        try {
            User user = getByEmail(userDto.getEmail());
            if (userDto.getFirstName() != null) user.setFirstName(userDto.getFirstName());
            if (userDto.getLastName() != null) user.setLastName(userDto.getLastName());
            if (userDto.getEmail() != null) user.setEmail(userDto.getEmail());
            if (userDto.getPassword() != null) user.setPassword(userDto.getPassword());
            Long address_id = user.getAddress().getId();
            if (userDto.getAddress() != null) {
                AddressDto adresDto = userDto.getAddress();
                Address address = addressRepository.getReferenceById(address_id);
                if (adresDto.getCountry() != null) address.setCountry(adresDto.getCountry());
                if (adresDto.getCity() != null) address.setCity(adresDto.getCity());
                if (adresDto.getStreet() != null) address.setStreet(adresDto.getStreet());
                if (adresDto.getApartment() != null) address.setApartment(adresDto.getApartment());
                if (adresDto.getZipCode() != null) address.setZipCode(adresDto.getZipCode());
                user.setAddress(address);
            }
            repository.save(user);
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void checkByEmailAndsendCinfirmationMessage(UserDto userDto) {
        try {
            User user = validateAndMap(userDto);
            mailSenderClient.send(userDto.getEmail(), "Confirmation Message", "Please confirm your registration by clicking on the link");
            repository.save(user);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private User validateAndMap(UserDto userDto) {
        try {
            requestValidator.validateEmptyData(userDto);
            User user = getByEmail(userDto.getEmail());
            if (user == null) {
                AddressDto addressDto = userDto.getAddress();
                List<Address> addressList = addressRepository.findByParams(addressDto.getCountry()
                        , addressDto.getCity()
                        , addressDto.getStreet());
                return UserDataMapper.mapDtoToUser(userDto, addressList);
            } else throw new DublicateEmailException(displayKey.getValue("DUBLICATE_EMAIL"));
        } catch (ConstraintViolationException ex) {
            throw new InvalidValuePassedException(displayKey.getValue("INVALID_VALUE") + "\n" + ex.getMessage());
        } catch (UserNotFoundException e) {
            throw new RuntimeException(e);
        } catch (DataIntegrityViolationException ex) {
            throw new DublicateEmailException(displayKey.getValue("DUBLICATE_EMAIL"));
        } catch (RuntimeException ex) {
            throw ex;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public UserDto enableUserByEmail(String email) {
        UserDto userDto;
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(email);
            String emailValue = new String(decodedBytes);
            User user = repository.findByEmail(emailValue);
            user.setStatus(Status.ENABLED);
            userDto = UserDataMapper.mapUserToDto(user);
            repository.save(user);
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw e;
        }
        return userDto;
    }

    @Override
    public ArrayList<User> getDisabledUsers(String status) {
        return repository.getDisabledUsers(status);
    }

    @Override
    public void removeUser(User user) {
        repository.delete(user);
    }

    @Override
    public String getToken(String email, Role role, JwtProvider jwtProvider) {
        return jwtProvider.createToken(email, role.name());
    }

    @Override
    public User getUserByToken(String token, JwtProvider jwtProvider) throws UserNotFoundException {
        String email = jwtProvider.getEmail(token);
        return getByEmail(email);
    }

    @Override
    public User getByEmail(String email) throws UserNotFoundException {
        User user = repository.findByEmail(email);
        if (user != null) {
            return user;
        } else throw new UserNotFoundException("There is no user with this email");
    }


}
