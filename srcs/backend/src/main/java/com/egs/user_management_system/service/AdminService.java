package com.egs.user_management_system.service;

import com.egs.user_management_system.model.dto.UserDto;
import com.egs.user_management_system.util.exception.UserNotFoundException;

import java.io.File;
import java.util.List;

public interface AdminService {
    List<UserDto> createUsersFromCSVFile(File scv);

    void delete(String email) throws UserNotFoundException;

    void update(UserDto userDto) throws UserNotFoundException;
}
