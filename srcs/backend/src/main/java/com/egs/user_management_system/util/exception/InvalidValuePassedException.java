package com.egs.user_management_system.util.exception;

public class InvalidValuePassedException extends RuntimeException {
    public InvalidValuePassedException(String message) {
        super(message);
    }
}
