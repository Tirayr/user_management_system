package com.egs.user_management_system.util.annotation;

import org.passay.AbstractCharacterRule;
import org.passay.PasswordUtils;

public class CustomSpecialCharacterRule extends AbstractCharacterRule {

    public static final String CHARS = "!#$%*";
    public static final String ERROR_CODE = "INSUFFICIENT_SPECIAL";

    public CustomSpecialCharacterRule() {
    }

    public CustomSpecialCharacterRule(int num) {
        this.setNumberOfCharacters(num);
    }

    @Override
    protected String getCharacterTypes(String s) {
        return PasswordUtils.getMatchingCharacters(CHARS, s);
    }

    @Override
    protected String getErrorCode() {
        return ERROR_CODE;
    }

    @Override
    public String getValidCharacters() {
        return CHARS;
    }
}